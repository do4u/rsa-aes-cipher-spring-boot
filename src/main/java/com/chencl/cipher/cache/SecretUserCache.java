package com.chencl.cipher.cache;

import java.util.HashMap;

import com.chencl.cipher.entity.SecretKeyEntity;

/**
 * 使用场景为项目需要跟第三方接口对接时使用
 * 引用项目启动时加载发送方接收方公私秘钥
 * 入口网关层集群部署时，数据库配置信息有改动时，每台机都要刷新
 * @ClassName:  SecretUserCache   
 * @Description: 加解密用户缓存
 * @author: Ccl
 * @date:   2021年11月27日 下午3:15:39      
 * @Copyright:
 */
public class SecretUserCache {
	
	public static HashMap<String, SecretKeyEntity> userCache = new HashMap<>();
	
	public static SecretKeyEntity getByAppid(String appid){
		return userCache.get(appid);
	}

}
