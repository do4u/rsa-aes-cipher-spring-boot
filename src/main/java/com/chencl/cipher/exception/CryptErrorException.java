package com.chencl.cipher.exception;

/**
 * @ClassName:  CryptErrorException   
 * @Description: 加解密过程抛出的异常，在项目全局中捕获，并按项目的要求进行提示
 * @author: Ccl
 * @date:   2021年11月27日 下午3:49:31      
 * @Copyright:
 */
public class CryptErrorException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public CryptErrorException(String message) {
		super(message);
	}

}
