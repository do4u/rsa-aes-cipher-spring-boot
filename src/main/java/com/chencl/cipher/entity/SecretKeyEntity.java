package com.chencl.cipher.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName:  SecretKeyEntity   
 * @Description:加解密实体
 * @author: Ccl
 * @date:   2021年11月27日 下午2:25:56      
 * @Copyright:
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecretKeyEntity {
	
	/**
	 * appid用户标识
	 */
	private String appid;
	/**
	 * 私钥
	 */
	private String privateKey;
	/**
	 * 公钥
	 */
	private String publicKey;
	/**
	 * 编码
	 */
	private String charset = "UTF-8";

}
