package com.chencl.cipher.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * 从配置中获取配置数据
 * @ClassName:  SecretKeyEntity   
 * @Description:加解密配置参数
 * @author: Ccl
 * @date:   2021年11月27日 下午2:25:56      
 * @Copyright:
 */
@Data
@ConfigurationProperties(prefix = "rsa.encrypt")
@Configuration
public class SecretKeyConfig {
	
	/**
	 * appid用户标识
	 */
	private String appid;
	/**
	 * 是否开启加解密日志
	 */
	private boolean showLog = false;

}
