package com.chencl.cipher.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseBodyDTO {
	
	private String code;
	private String msg;
	private Object data;
	
	public static ResponseBodyDTO buildSuccess(Object data){
		return buildSuccess("ok",data);
	}
	
	public static ResponseBodyDTO buildSuccess(String msg,Object data){
		return ResponseBodyDTO.builder().code("200").msg(msg).data(data).build();
	}
	
	public static ResponseBodyDTO buildFail(String code,String msg,Object data){
		return ResponseBodyDTO.builder().code(code).msg(msg).data(data).build();
	}
}
