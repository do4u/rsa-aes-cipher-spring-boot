package com.chencl.cipher.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecretDataDTO {
	
	/**
	 * 用户标识
	 */
	private String appid;
	/**
	 * 加密数据
	 */
	private String content;
	/**
	 * 签名
	 */
	private String sign;
	/**
	 * 随机秘钥
	 */
	private String random_key;
}
