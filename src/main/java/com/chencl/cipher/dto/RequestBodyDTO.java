package com.chencl.cipher.dto;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class RequestBodyDTO extends SecretDataDTO{
	
	private String test1;
	private String test2;
	private List<Map<String,Object>> list;

}
