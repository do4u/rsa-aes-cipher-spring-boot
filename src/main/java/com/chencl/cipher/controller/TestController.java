package com.chencl.cipher.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.chencl.cipher.annotation.Decrypt;
import com.chencl.cipher.annotation.Encrypt;
import com.chencl.cipher.cache.SecretUserCache;
import com.chencl.cipher.config.SecretKeyConfig;
import com.chencl.cipher.dto.RequestBodyDTO;
import com.chencl.cipher.dto.ResponseBodyDTO;
import com.chencl.cipher.dto.SecretDataDTO;
import com.chencl.cipher.entity.SecretKeyEntity;
import com.chencl.cipher.util.AESUtils;
import com.chencl.cipher.util.MD5Util;
import com.chencl.cipher.util.RSAUtils;
import com.chencl.cipher.util.RandomUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/test")
@Validated
public class TestController {
	
	@Autowired
	SecretKeyConfig secretKeyConfig;
	
	@PostConstruct
	@PostMapping(value = "/init")
	public void init(){
		log.info("初始化开始-------------------");
		
		Map<String,String> resKey1 = RSAUtils.getRsaKey();
		Map<String,String> resKey2 = RSAUtils.getRsaKey();
		
		SecretKeyEntity entity1 = SecretKeyEntity.builder().appid("123456").publicKey(resKey1.get("publiceKey")).privateKey(resKey1.get("privateKey")).build();
		SecretKeyEntity entity2 = SecretKeyEntity.builder().appid(secretKeyConfig.getAppid()).publicKey(resKey2.get("publiceKey")).privateKey(resKey2.get("privateKey")).build();
		
		SecretUserCache.userCache.put(entity1.getAppid(), entity1);
		SecretUserCache.userCache.put(entity2.getAppid(), entity2);
		
		log.info("初始化结束-------------------");
	}
	
	@Decrypt
	@Encrypt
	@PostMapping(value = "/ask")
    public ResponseBodyDTO ask(@RequestBody RequestBodyDTO data){
    	log.info("测试接口,请求参数:[{}]",data);
    	Map<String,Object> retMap = new HashMap<>();
    	retMap.put("test1","test123");
        return ResponseBodyDTO.buildSuccess(retMap);
    }
	
	@Decrypt
	@PostMapping(value = "/ask2")
    public ResponseBodyDTO ask2(@RequestBody RequestBodyDTO data){
    	log.info("测试接口,请求参数:[{}]",data);
    	Map<String,Object> retMap = new HashMap<>();
    	retMap.put("test1","test123");
        return ResponseBodyDTO.buildSuccess(retMap);
    }
	
	@PostMapping(value = "/ask3")
    public ResponseBodyDTO ask3(@RequestBody RequestBodyDTO data){
    	log.info("测试接口,请求参数:[{}]",data);
    	Map<String,Object> retMap = new HashMap<>();
    	retMap.put("test1","test123");
        return ResponseBodyDTO.buildSuccess(retMap);
    }
	
	@PostMapping(value = "/get")
    public ResponseBodyDTO get(){
		String content = "";
		String sign = "";
		String random_key = "";
		try {
			List<Map<String,Object>> list = new ArrayList<>();
			Map<String,Object> map1 = new HashMap<>();
			map1.put("test3", "789");
			list.add(map1);
			RequestBodyDTO entity = new RequestBodyDTO();
			entity.setTest1("123");
			entity.setTest2("456");
			entity.setList(list);
			String data = JSONObject.toJSONString(entity);
			
			SecretKeyEntity sender = SecretUserCache.getByAppid("123456");
			SecretKeyEntity home = SecretUserCache.getByAppid(secretKeyConfig.getAppid());
			
			String aesKey = RandomUtils.getUUIDRandomTrim();
			content = AESUtils.encrypt(data, aesKey);
			String content_digest = MD5Util.MD5Encode(data);
			sign = RSAUtils.encryptByPrivateKey(content_digest.getBytes("UTF-8"),sender.getPrivateKey());
			random_key = RSAUtils.encryptByPublicKey(aesKey.getBytes("UTF-8"),home.getPublicKey());
			
			Map<String,Object> retMap = new HashMap<>();
			retMap.put("appid","123456");
	    	retMap.put("content",URLEncoder.encode(content,"utf-8"));
	    	retMap.put("sign",URLEncoder.encode(sign,"utf-8"));
	    	retMap.put("random_key",URLEncoder.encode(random_key,"utf-8"));
	        return ResponseBodyDTO.buildSuccess(retMap);
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 return ResponseBodyDTO.buildSuccess("");
    }
	

	@PostMapping(value = "/decode")
    public ResponseBodyDTO decode(@RequestBody SecretDataDTO entity) throws UnsupportedEncodingException{
		String appid = entity.getAppid();
		String content = URLDecoder.decode(entity.getContent(),"utf-8");
		String sign = URLDecoder.decode(entity.getSign(),"utf-8");
		String random_key = URLDecoder.decode(entity.getRandom_key(),"utf-8");
		
		SecretKeyEntity sender = SecretUserCache.getByAppid(appid);
		SecretKeyEntity home = SecretUserCache.getByAppid(secretKeyConfig.getAppid());
		
		String decode_random_key = "";
		String decode_content = "";
		String decode_sign = "";

		try {
			decode_random_key = RSAUtils.decryptByPrivateKey(Base64.decodeBase64(random_key.getBytes("UTF-8")),home.getPrivateKey());
		    decode_content = AESUtils.decrypt(content, decode_random_key);
		    decode_sign = RSAUtils.decryptByPublicKey(Base64.decodeBase64(sign.getBytes("UTF-8")),sender.getPublicKey());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean isVerifi = MD5Util.MD5Encode(decode_content).equals(decode_sign);
		
		Map<String,Object> retMap = new HashMap<>();
		retMap.put("isVerifi",isVerifi);
		retMap.put("decode_content_digest",MD5Util.MD5Encode(decode_content));
		retMap.put("decode_random_key",decode_random_key);
		retMap.put("decode_content",decode_content);
		retMap.put("decode_sign",decode_sign);
		return ResponseBodyDTO.buildSuccess(retMap);
	}
	

}
