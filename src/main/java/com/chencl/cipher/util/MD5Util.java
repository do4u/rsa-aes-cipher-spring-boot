package com.chencl.cipher.util;

import java.security.MessageDigest;

/**
 * @ClassName:  MD5Util   
 * @Description: 生成内容摘要  
 * @author: Ccl
 * @date:   2021年11月25日 上午11:46:14      
 * @Copyright:
 */
public class MD5Util {
	
	/**
	 * @Title: MD5Encode   
	 * @Description:  生成内容摘要  
	 * @param: @param origin 数据源
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	public static String MD5Encode(String origin) {
        return MD5Encode(origin,"");
    }

	/**
	 * @Title: MD5Encode   
	 * @Description: 生成内容摘要   
	 * @param: @param origin 数据源
	 * @param: @param charsetname 编码
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
    public static String MD5Encode(String origin, String charsetname) {
        String resultString = null;
        try {
            resultString = new String(origin);
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (charsetname == null || "".equals(charsetname))
                resultString = byteArrayToHexString(md.digest(resultString
                        .getBytes()));
            else
                resultString = byteArrayToHexString(md.digest(resultString
                        .getBytes(charsetname)));
        } catch (Exception exception) {
        }
        return resultString;
    }

    private static String byteArrayToHexString(byte b[]) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++)
            resultSb.append(byteToHexString(b[i]));

        return resultSb.toString();
    }

    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0)
            n += 256;
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }
    
    private static final String hexDigits[] = {"0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
}
