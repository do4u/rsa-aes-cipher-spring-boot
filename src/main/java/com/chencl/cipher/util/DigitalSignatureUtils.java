package com.chencl.cipher.util;

/**
 * 应用于公钥加密体系，用发送方的私钥加密内容摘要，接受者根据发送方的公钥解密并校验内容
 * 发送者不可抵赖，传输内容被截获，无法篡改
 * @ClassName:  DigitalSignatureUtils   
 * @Description: 数字签名
 * @author: Ccl
 * @date:   2021年11月25日 上午11:36:20      
 * @Copyright:
 */
public class DigitalSignatureUtils {
	
	/**
	 * 
	 * @Title: createDigitalSignature   
	 * @Description: 生成数字签名
	 * @param: @param content
	 * @param: @param privateKey
	 * @param: @return      
	 * @return: String      
	 * @throws
	 */
	public static String createDigitalSignature(String content,String privateKey){
		String contentDigest = MD5Util.MD5Encode(content);
		try {
			return RSAUtils.sign(contentDigest.getBytes(), privateKey);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

}
