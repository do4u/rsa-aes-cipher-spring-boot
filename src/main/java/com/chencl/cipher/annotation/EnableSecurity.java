package com.chencl.cipher.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

import com.chencl.cipher.annotation.aspect.De2EncryptAspect;
import com.chencl.cipher.config.SecretKeyConfig;

/**
 * 
 * @ClassName:  EnableSecurity   
 * @Description: 开启或关闭注解加解密
 * @author: Ccl
 * @date:   2021年11月27日 下午4:18:37      
 * @Copyright:
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Import({SecretKeyConfig.class,
		De2EncryptAspect.class})
public @interface EnableSecurity{

}
