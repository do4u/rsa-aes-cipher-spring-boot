package com.chencl.cipher.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * 
 * @ClassName:  Decrypt   
 * @Description: 解密注解
 * @author: Ccl
 * @date:   2021年1月12日 上午11:25:08      
 * @Copyright:
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Decrypt {

}
