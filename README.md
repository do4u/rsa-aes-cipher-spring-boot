# rsa-aes-cipher-spring-boot

#### 介绍
采用RSA与AES,用注解及切面的方式实现与第三方接口交互数据加密及解密过程

解密及验证过程如下
    1、用接受者的私钥解密随机秘钥
    2、用解密出来的随机秘钥解密加密原文
    3、用发送方的公钥解密数字签名，获取原文摘要
    4、用第2步解密出来的原文生成摘要与第3步获取到的原文摘要进行比较判断内容是否被篡改，内容一致则验签通过
 
加密过程
    1、生成随机秘钥
    2、对发送数据生成消息摘要
    3、AES对称加密发送数据
    4、消息摘要用己方（发送方）私钥加密作为数字签名
    5、随机秘钥用接收方公钥加密

#### 软件架构
采用Spring框架，可创建独立，生产级的spring应用程序，具有轻量级、非侵入性方法、松散耦合、模块化、易于测试和MVC框架等特点

核心容器包括：
    1、CORE：核心模块提供了弹簧框架的基本功能，如IoC和DI。
    2、Bean： Bean模块提供BeanFactory。
    3、Context：上下文模块提供了一种访问任何对象的方法。ApplicationContext接口是Context模块的主要部分。
    4、Expression language：表达式语言模块提供了一种在运行时操作对象的方法。

数据访问/集成包含以下内容：
    1、JDBC： JDBC模块提供JDBC抽象层。
    2、ORM： ORM为对象关系映射API提供集成层，如JPA和Hibernate等
    3、OXM： OXM模块为对象/ XML映射API提供抽象层，如JAXB，Castor和XMLBeans等
    4、JMS： JMS模块提供消息处理功能。
    5、Transaction：事物模块为POJO等类提供交易管理功能。

网站：
    Web模块由Web，Web-Servlet，Web-Struts，Web-Socket和Web-Portlet组成，它们提供了创建Web应用程序的功能。

AOP：
    AOP模块提供面向方面的编程实现，它提供了定义方法拦截器的工具。

仪器仪表：
    Instrumentation模块提供类检测支持和类加载器实现
    Spring IoC容器负责在整个生命周期中创建，连接，配置和管理对象。它使用配置元数据来创建，配置和管理对象。配置元数据可以由spring配置xml文件或注释表示。

Spring IoC容器的类型：
    BeanFactory：BeanFactory org.springframework.beans.factory.BeanFactory是接口，XmlBeanFactory是它的实现类。它是一个简单的容器，为依赖注入提供基本支持。

    BeanFactory语法：
        Resource resource = new ClassPathResource(“spring configuration file”);
        BeanFactory beanFactory = new XmlBeanFactory(resource);

    ApplicationContext：
        ApplicationContext org.springframework.context.ApplicationContext是接口，ClassPathXmlApplicationContext是它的实现类。
        ApplicationContext容器包含BeanFactory容器的所有功能，以及一些额外的功能，如国际化，事件监听器等。

    ApplicationContext语法：
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext （“spring configuration file” ）;

#### 安装教程

1.  由于该加解密使用场景为与三方系统消息集成时所用，自己的公私钥及三方的公钥等加载在内存，支持动态配置三方appid与公钥，只需要刷新缓存即可生效
![输入图片说明](https://images.gitee.com/uploads/images/2021/1129/105002_753b2e9f_2303551.png "屏幕截图.png")
可从数据库获取配置并加载到内存，也可改成缓存到redis，方便集群化部署时访问

#### 使用说明

1.在需要解密的方法上面加上@Decrypt注解，如果返回数据需要加密加上@Encrypt注解
![输入图片说明](https://images.gitee.com/uploads/images/2021/1129/105321_429e2aa5_2303551.png "屏幕截图.png")

2.注解切面实现请看De2EncryptAspect类

3.在配置文件中（application-*.yml）配置本机appid
